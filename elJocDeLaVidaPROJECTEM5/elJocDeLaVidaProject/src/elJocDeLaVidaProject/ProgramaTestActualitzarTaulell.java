package elJocDeLaVidaProject;

import static org.junit.Assert.*;
import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/**
 * Test al m�tode "Actualitzar" de la classe "Programa"
 * @author arnau
 * @version 1.0
 */

@RunWith(Parameterized.class)
public class ProgramaTestActualitzarTaulell {
	
	private String[][] taulellNou;
	private String[][] taulellAntic;
	private int files;
	private int columnes;

	private static String[][] solucio;
	
	/**
	 * M�tode dedicat a indicar quines seran les entrades per al test
	 * @param taulellNou taulell actual que farem servir com a model per la copia
	 * @param taulellAntic taulell que volem actualitzar 
	 * @param files numero de files
	 * @param columnes numero de columnes
	 * @param solucio solucio que ha de donar el test si es correcte
	 */
	
	public ProgramaTestActualitzarTaulell (String[][] taulellNou, String[][] taulellAntic, int files, int columnes, String[][] solucio) {
		
		this.taulellNou = taulellNou;
		this.taulellAntic = taulellAntic;
		this.files = files;
		this.columnes = columnes;
		
		this.solucio = solucio;
		
	}
	
	/**
	 * Funci� encarregada de subministrar els casos de prova.
	 * @return Retorna cada cas de prova que executarem per comprovar el funcionament del programa.
	 */

	@Parameters
	public static Collection<Object[]> numeros() {
		
		/*Test 1*/
		String[][] exAntic1 = {{"M","M","M","M","M","M","M","M","M","M"},
							  {"M","M","M","V","V","V","M","M","M","M"},
							  {"M","M","M","M","M","M","M","M","M","M"},
							  {"M","V","M","M","M","M","M","V","M","M"},
							  {"M","V","M","M","M","M","M","V","M","M"},
							  {"M","V","M","M","M","M","M","V","M","M"},
							  {"M","M","M","M","M","M","M","M","M","M"},
							  {"M","M","M","V","V","V","M","M","M","M"},
							  {"M","M","M","M","M","M","M","M","M","M"},
							  {"M","M","M","M","M","M","M","M","M","M"}};
	
		String[][] exTaulellNouISolucio1 = {{"M","M","M","M","M","M","V","M","M","M"},
										   {"M","M","M","V","M","V","M","M","M","V"},
										   {"V","M","M","V","M","M","V","M","V","M"},
										   {"M","V","M","M","M","M","M","M","M","M"},
										   {"M","M","M","M","M","V","V","V","M","V"},
										   {"M","V","M","M","M","M","V","V","M","M"},
										   {"M","V","M","M","M","M","M","M","V","M"},
										   {"V","M","V","V","M","V","M","M","M","M"},
										   {"V","V","M","M","M","M","M","M","M","M"},
										   {"M","M","V","M","M","V","V","M","M","M"}};
		
		/*Test 2*/
		
		String[][] exAntic2 = {{"M","M","M","M","M","M","M","M","M","M"},
				  		  	   {"M","M","M","V","V","V","M","M","M","M"},
						  	   {"M","M","M","M","M","M","M","M","M","M"},
						  	   {"M","V","M","M","M","M","M","V","M","M"},
						 	   {"M","V","M","M","M","M","M","V","M","M"},
						 	   {"M","V","M","M","M","M","M","V","M","M"},
						 	   {"M","M","M","M","M","M","M","M","M","M"},
						 	   {"M","M","M","V","V","V","M","M","M","M"},
						 	   {"M","M","M","M","M","M","M","M","M","M"},
						 	   {"M","M","M","M","M","M","M","M","M","M"}};

		String[][] exTaulellNouISolucio2 = {{"M","M","M","M","M","M","V","M","M","M"},
										   	{"M","M","M","V","M","V","M","M","M","V"},
										   	{"V","M","M","V","M","M","V","M","V","M"},
										   	{"M","V","M","M","M","M","M","M","M","M"},
										   	{"M","M","M","M","M","V","V","V","M","V"},
										   	{"M","V","M","M","M","M","V","V","M","M"},
										   	{"M","V","M","M","M","M","M","M","V","M"},
										   	{"V","M","V","V","M","V","M","M","M","M"},
										   	{"V","V","M","M","M","M","M","M","M","M"},
										   	{"M","M","V","M","M","V","V","M","M","M"}};
		
		/*Test 3*/
		
		String[][] exAntic3 = {{"M","M","M","V","V"},
							   {"M","V","M","M","M"},
							   {"M","V","M","V","M"},
							   {"M","M","M","M","V"}};

		String[][] exTaulellNouISolucio3 = {{"V","M","V","V","V"},
										    {"M","M","M","V","V"},
										    {"M","V","V","M","V"},
										    {"M","V","M","V","V"},
										    {"V","M","V","V","V"}};
		
		/*Test 4*/
		
		String[][] exAntic4 = {{"M","M","M","M","M","M","M","M","M","M"},
							   {"M","M","M","V","V","V","M","M","M","M"},
							   {"M","M","M","M","M","M","M","M","M","M"},
							   {"M","V","M","M","M","M","M","V","M","M"},
							   {"M","V","M","M","M","M","M","V","M","M"},
							   {"M","V","M","M","M","M","M","V","M","M"},
							   {"M","M","M","M","M","M","M","M","M","M"},
							   {"M","M","M","V","V","V","M","M","M","M"},
							   {"M","M","M","M","M","M","M","M","M","M"},
							   {"M","M","M","M","M","M","M","M","M","M"}};

		String[][] exTaulellNouISolucio4 = {{"M","M","M","M","M","M","V","M","M","M"},
										   	{"M","M","M","V","M","V","M","M","M","V"},
										   	{"V","M","M","V","M","M","V","M","V","M"},
										   	{"M","V","M","M","M","M","M","M","M","M"},
										   	{"M","M","M","M","M","V","V","V","M","V"},
										   	{"M","V","M","M","M","M","V","V","M","M"},
										   	{"M","V","M","M","M","M","M","M","V","M"},
										   	{"V","M","V","V","M","V","M","M","M","M"},
										   	{"V","V","M","M","M","M","M","M","M","M"},
										   	{"M","M","V","M","M","V","V","M","M","M"}};
		
		/*Test 5*/
		
		String[][] exAntic5 = {{"V","V","V","M","M","M","M","M","M","M"},
							   {"V","V","V","V","M","V","M","M","M","M"},
							   {"V","M","M","M","M","M","V","M","V","M"},
							   {"M","M","V","M","M","M","V","M","M","V"},
							   {"M","M","M","M","M","V","V","M","M","V"},
							   {"V","M","V","M","V","V","V","M","M","M"},
							   {"V","M","V","M","V","V","M","M","V","V"},
							   {"V","M","V","M","M","V","M","M","M","V"},
							   {"V","V","M","V","M","M","V","M","V","V"},
							   {"M","M","V","M","M","V","V","M","M","M"}};
		
		String[][] exTaulellNouISolucio5 = {{"M","M","M","M","M","M","V","M","M","M"},
										   	{"M","M","M","V","M","V","M","M","M","M"},
										   	{"M","M","M","V","M","M","V","M","M","M"},
										   	{"M","V","M","M","M","M","M","M","M","M"},
										   	{"M","M","M","M","M","M","V","M","M","V"},
										   	{"M","M","M","M","M","M","V","M","M","M"},
										   	{"M","M","M","M","M","M","M","M","M","M"},
										   	{"M","M","M","V","M","V","M","M","M","M"},
										   	{"M","V","M","M","M","M","M","M","M","M"},
										   	{"M","M","V","M","M","V","M","M","M","M"}};
		
		/*Test 6*/
		
		String[][] exAntic6 = {{"V","V","M","M"},
								{"M","V","M","M"},
								{"M","V","M","V"},
								{"V","M","V","V"},
								{"V","M","M","V"}};
		
		String[][] exTaulellNouISolucio6 = {{"V","V","M","V"},
											{"M","M","M","M"},
											{"M","V","M","V"},
											{"V","M","M","M"},
											{"M","M","M","M"}};
		
		/*Test 7*/
		
		String[][] exAntic7 = {{"V","M","V","M"},
							   {"M","V","M","M"},
							   {"M","V","M","V"},
							   {"V","M","V","V"},
							   {"V","M","V","V"},
							   {"M","M","M","M"}};
		
		String[][] exTaulellNouISolucio7 = {{"M","M","M","M"},
										    {"M","V","M","V"},
										    {"M","M","V","V"},
										    {"M","V","M","M"},
									    	{"M","M","V","V"},
									    	{"V","V","M","M"}};
		
		/*Test 8*/
		
		String[][] exAntic8 = {{"M","V","V","M"},
							   {"M","V","V","M"},
							   {"M","M","M","M"},
							   {"M","V","M","V"}};
		
		String[][] exTaulellNouISolucio8 = {{"V","M","M","M"},
										    {"M","M","M","V"},
										    {"V","M","M","V"},
										    {"V","M","M","M"}};

		/*Test 9*/
		
		String[][] exAntic9 = {{"M","M","M","V","M","V","M"},
							   {"V","M","V","V","V","M","M"},
							   {"V","M","V","M","V","M","M"},
							   {"M","V","M","M","M","V","V"},
							   {"M","V","M","V","V","M","M"},
							   {"M","V","M","M","M","V","M"},
							   {"M","M","M","M","M","M","V"},
							   {"M","V","V","M","M","V","V"}};
		
		String[][] exTaulellNouISolucio9 = {{"V","V","V","M","M","M","M"},
										    {"V","M","V","M","V","M","M"},
										    {"V","V","V","V","V","V","M"},
										    {"M","V","M","M","V","V","V"},
										    {"M","V","M","V","V","M","M"},
										    {"V","M","V","M","V","V","V"},
										    {"V","M","M","M","V","M","M"},
										    {"M","M","M","M","M","M","M"}};
		
		/*Test 10*/
		
		String[][] exAntic10 ={{"M","M","V","M","V"},
							   {"M","M","M","M","M"},
							   {"M","V","M","M","V"},
							   {"V","M","V","V","M"}};
		
		String[][] exTaulellNouISolucio10 ={{"V","V","V","M","M"},
										    {"M","V","M","V","V"},
										    {"V","M","M","V","M"},
										    {"M","M","M","V","M"}};
		
		
		
		
		return Arrays.asList(new Object[][] {
			{exAntic1, exTaulellNouISolucio1, 10, 10, exTaulellNouISolucio1},
			{exAntic2, exTaulellNouISolucio2, 10, 10, exTaulellNouISolucio2},
			{exAntic3, exTaulellNouISolucio3, 4, 4, exTaulellNouISolucio3},
			{exAntic4, exTaulellNouISolucio4, 10, 10, exTaulellNouISolucio4},
			{exAntic5, exTaulellNouISolucio5, 10, 10, exTaulellNouISolucio5},
			{exAntic6, exTaulellNouISolucio6, 5, 4, exTaulellNouISolucio6},
			{exAntic7, exTaulellNouISolucio7, 6, 4, exTaulellNouISolucio7},
			{exAntic8, exTaulellNouISolucio8, 4, 4, exTaulellNouISolucio8},
			{exAntic9, exTaulellNouISolucio9, 8, 7, exTaulellNouISolucio9},
			{exAntic10, exTaulellNouISolucio10, 4, 5, exTaulellNouISolucio10}});
		
	}
	
	/**
	 * Funci� on es compara la soluci� que aporta el m�tode que estem comprovant, amb la soluci� que li donem nosaltres.
	 */

	@Test
	public void test() {
		String[][] res = Programa.actualitzar(taulellNou, taulellAntic, files, columnes);
		assertArrayEquals(solucio,  res);
	}
	
}











