package elJocDeLaVidaProject;

import static org.junit.Assert.*;
import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/**
 * Test al m�tode "Iterar" de la classe "Programa"
 * @author arnau
 * @version 1.0
 */

@RunWith(Parameterized.class)
public class ProgramaTestIterar {
	
	private String[][] taulellAntic;
	private int files;
	private int columnes;
	private int numeroIteracions;

	private String[][] solucio;
	
	/**
	 * M�tode dedicat a indicar quines seran les entrades per al test
	 * @param taulellAntic taulell anterior el qual el fem servir com a base per iterar.
	 * @param files numero de files.
	 * @param columnes numero de columnes.
	 * @param numeroIteracions quantitat d'iteracions que volem fer.
	 * @param solucio taulell que farem servir per comprovar el funcionament correcte
	 */
	
	public ProgramaTestIterar (String[][] taulellAntic, int files, int columnes, int numeroIteracions, String[][] solucio) {
		
		this.taulellAntic = taulellAntic;
		this.files = files;
		this.columnes = columnes;
		this.numeroIteracions = numeroIteracions;
		this.solucio = solucio;
		
	}
	
	/**
	 * Funci� encarregada de subministrar els casos de prova.
	 * @return Retorna cada cas de prova que executarem per comprovar el funcionament del programa.
	 */

	@Parameters
	public static Collection<Object[]> numeros() {
		
		/*Test 1*/
		
		String[][] exAntic1 = {{"M","M","M","M","M","M"},
							   {"M","M","M","V","V","V"},
							   {"M","M","M","M","M","M"},
							   {"M","M","V","M","M","M"},
							   {"M","V","M","M","M","M"},
							   {"M","V","M","M","M","M"}};
	
		String[][] exTaulellNouISolucio1 = {{"M","M","M","M","V","M"},
										    {"M","M","M","M","V","M"},
										    {"M","M","M","V","V","M"},
										    {"M","M","M","M","M","M"},
										    {"M","V","V","M","M","M"},
										    {"M","M","M","M","M","M"}};
		
		/*Test 2*/
		
		String[][] exAntic2 = {{"M","M","V","M","M","M","M","M","M"},
							   {"M","M","M","M","M","M","M","M","M"},
							   {"V","M","M","M","M","V","M","V","M"},
							   {"V","V","V","V","M","M","M","V","M"}};

		String[][] exTaulellNouISolucio2 = {{"M","M","M","M","M","M","M","M","M"},
											{"M","M","M","M","M","M","M","M","M"},
											{"V","M","V","M","M","M","V","M","M"},
											{"V","V","V","M","M","M","V","M","M"}};
		
		/*Test 3*/
		
		String[][] exAntic3 = {{"V","M","V","V","M","M","V","M"},
							   {"M","V","V","M","M","V","M","V"},
							   {"M","V","M","M","V","M","M","M"},
							   {"M","M","M","V","V","M","V","V"}};

		String[][] exTaulellNouISolucio3 = {{"M","V","V","M","M","M","M","M"},
											{"M","V","V","M","M","M","M","M"},
											{"M","M","M","M","M","M","M","M"},
											{"M","M","M","M","M","M","M","M"}};
		
		/*Test 4*/
		
		String[][] exAntic4 = {{"M","V","M","V","M"},
							   {"V","M","M","V","M"},
							   {"M","V","M","M","M"},
							   {"V","V","V","M","V"}};

		String[][] exTaulellNouISolucio4 = {{"M","V","V","M","M"},
											{"M","V","V","M","M"},
											{"M","M","M","M","M"},
											{"M","M","M","M","M"}};
		
		/*Test 5*/
		
		String[][] exAntic5 = {{"V","M","M","M","M"},
							   {"V","V","V","V","M"},
							   {"V","V","V","V","M"},
							   {"M","V","V","M","M"},
							   {"M","V","V","M","M"},
							   {"V","M","M","V","M"},
							   {"M","V","M","M","M"}};
			
		String[][] exTaulellNouISolucio5 = {{"M","M","M","M","M"},
											{"M","M","M","M","M"},
											{"M","M","M","M","M"},
											{"M","M","M","M","M"},
											{"M","M","M","M","M"},
											{"M","M","M","M","M"},
											{"M","M","M","M","M"}};
		
		/*Test 6*/
		
		String[][] exAntic6 = 	{{"M","V","M","M","M","M"},
								{"V","M","M","M","M","M"},
								{"M","M","M","M","M","M"},
								{"M","V","M","M","V","V"},
								{"V","M","M","V","M","M"},
								{"M","M","M","V","V","M"},
								{"V","M","V","M","M","M"},
								{"M","V","M","M","M","M"}};

		String[][] exTaulellNouISolucio6 = {{"M","M","M","M","M","M"},
											{"M","M","M","M","M","M"},
											{"M","M","M","M","M","M"},
											{"M","M","M","M","V","M"},
											{"M","M","M","M","V","M"},
											{"M","M","M","M","M","M"},
											{"M","M","M","M","M","M"},
											{"M","M","M","M","M","M"}};
		
		/*Test 7*/
		
		String[][] exAntic7 = {{"M","M","V","M","M","M","V"},
							   {"M","M","M","M","M","V","V"},
							   {"V","V","V","M","V","V","M"},
							   {"V","M","M","V","M","M","M"}};

		String[][] exTaulellNouISolucio7 = {{"M","M","M","M","V","V","M"},
											{"M","M","M","M","V","V","M"},
											{"M","M","M","M","M","M","M"},
											{"M","M","M","M","M","M","M"}};
		
		/*Test 8*/
		
		String[][] exAntic8 = {{"M","M","V","V","V","M","M"},
							  {"M","M","V","M","M","V","V"},
							  {"M","M","M","V","M","M","M"},
							  {"V","M","V","V","V","M","M"},
							  {"M","M","M","M","M","V","M"},
							  {"M","M","M","M","V","V","M"},
							  {"V","V","V","V","M","M","V"},
							  {"M","M","M","M","M","V","V"},
							  {"M","M","M","M","V","M","M"}};

		String[][] exTaulellNouISolucio8 = {{"M","V","V","V","V","V","V"},
											{"V","V","M","V","V","M","V"},
											{"V","V","M","V","V","M","V"},
											{"V","V","M","M","M","M","V"},
											{"M","M","M","M","M","M","V"},
											{"M","M","M","M","M","M","M"},
											{"M","M","M","M","M","M","M"},
											{"V","M","V","M","M","V","V"},
											{"M","V","V","M","M","V","V"}};
			
		/*Test 9*/
		
		String[][] exAntic9 = {{"M","M","M","V","M"},
							  {"M","V","M","M","V"},
							  {"M","M","M","V","V"},
							  {"V","M","M","M","M"},
							  {"M","M","M","V","M"},
							  {"M","M","M","V","V"},
							  {"V","V","M","V","M"}};

		String[][] exTaulellNouISolucio9 = {{"M","M","M","M","M"},
											{"M","M","M","M","M"},
											{"M","M","M","V","M"},
											{"M","V","V","V","M"},
											{"M","M","M","M","M"},
											{"M","M","M","V","M"},
											{"M","M","M","M","M"}};
		
		/*Test 10*/
		
		String[][] exAntic10 = {{"M","V","M","V","M","M","V","M"},
								{"V","M","V","M","V","M","V","M"},
								{"M","M","V","V","M","M","V","V"},
								{"M","V","V","M","V","M","M","M"},
								{"V","M","M","M","V","V","M","V"},
								{"M","M","V","M","M","M","V","M"}};
			
		String[][] exTaulellNouISolucio10 = {{"M","M","M","V","M","V","V","M"},
											 {"M","M","V","M","V","M","V","M"},
											 {"M","M","M","M","M","M","M","M"},
											 {"V","M","M","M","V","M","V","V"},
											 {"M","V","M","M","V","M","V","V"},
											 {"M","M","V","V","V","V","V","M"}};



					
		return Arrays.asList(new Object[][] {
			{exAntic1, 6, 6, 1, exTaulellNouISolucio1},
			{exAntic2, 4, 9, 1, exTaulellNouISolucio2},
			{exAntic3, 4, 8, 7, exTaulellNouISolucio3},
			{exAntic4, 4, 5, 10, exTaulellNouISolucio4},
			{exAntic5, 7, 5, 6, exTaulellNouISolucio5},
			{exAntic6, 8, 6, 3, exTaulellNouISolucio6},
			{exAntic7, 4, 7, 7, exTaulellNouISolucio7},
			{exAntic8, 9, 7, 4, exTaulellNouISolucio8},
			{exAntic9, 7, 5, 3, exTaulellNouISolucio9},
			{exAntic10, 6, 8, 4, exTaulellNouISolucio10}
		});
		
	}

	/**
	 * Funci� on es compara la soluci� que aporta el m�tode que estem comprovant, amb la soluci� que li donem nosaltres.
	 */
	
	@Test
	public void test() {
		String[][] res = Programa.itarar(taulellAntic, files, columnes, numeroIteracions);
		assertArrayEquals(solucio,  res);
	}
	
}











