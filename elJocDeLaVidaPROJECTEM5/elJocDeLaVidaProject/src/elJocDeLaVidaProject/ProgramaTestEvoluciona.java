package elJocDeLaVidaProject;

import static org.junit.Assert.*;
import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/**
 * Test al m�tode "Evoluciona" de la classe "Programa"
 * @author arnau
 * @version 1.0
 */

@RunWith(Parameterized.class)
public class ProgramaTestEvoluciona {
	
	private String[][] taulellAntic;
	private int files;
	private int columnes;
	private int numeroIteracions;

	private String[][] solucio;
	
	/**
	 * M�tode dedicat a indicar quines seran les entrades per al test
	 * @param taulellAntic taulell que farem servir com a base per evolucionar el joc
	 * @param files numero de files
	 * @param columnes numero de columnes
	 * @param solucio solucio que ha de donar el test si es correcte
	 */
	
	public ProgramaTestEvoluciona (String[][] taulellAntic, int files, int columnes, String[][] solucio) {
		
		this.taulellAntic = taulellAntic;
		this.files = files;
		this.columnes = columnes;
		this.solucio = solucio;
		
	}
	
	/**
	 * Funci� encarregada de subministrar els casos de prova.
	 * @return Retorna cada cas de prova que executem per comprovar el funcionament del programa.
	 */
	@Parameters
	public static Collection<Object[]> numeros() {
		
		/*Test 1*/
		String[][] exAntic1 = {{"M","M","M","M","M","M"},
							   {"M","M","M","V","V","V"},
							   {"M","M","M","M","M","M"},
							   {"M","M","V","M","M","M"},
							   {"M","V","M","V","V","V"},
							   {"M","V","M","M","M","M"}};
	
		String[][] exTaulellNouISolucio1 = {{"M","M","M","M","V","M"},
										    {"M","M","M","M","V","M"},
										    {"M","M","M","V","V","M"},
										    {"M","M","V","V","V","M"},
										    {"M","V","M","V","V","M"},
										    {"M","M","V","M","V","M"}};
		
		/*Test 2*/
		
		String[][] exAntic2 = {{"M","V","M"},
						   	   {"V","V","V"},
						   	   {"M","M","M"},
						   	   {"V","V","V"}};

		String[][] exTaulellNouISolucio2 = {{"V","V","V"},
										    {"V","V","V"},
										    {"M","M","M"},
										    {"M","V","M"}};
		
		/*Test 3*/
		
		String[][] exAntic3 = {{"V","M","M","M","M","V","V"},
							   {"M","M","M","M","M","V","V"},
							   {"V","V","V","M","M","M","M"},
							   {"M","M","M","M","M","M","M"},
							   {"V","M","M","M","V","M","V"},
							   {"M","M","V","M","M","M","V"}};

		String[][] exTaulellNouISolucio3 = {{"M","M","M","M","M","V","V"},
											{"V","M","M","M","M","V","V"},
											{"M","V","M","M","M","M","M"},
											{"V","M","M","M","M","M","M"},
											{"M","M","M","M","M","V","M"},
											{"M","M","M","M","M","V","M"}};
		
		/*Test 4*/
		
		String[][] exAntic4 = {{"V","V","V","M","M","M"},
							   {"M","M","V","V","V","V"},
							   {"V","V","M","V","V","M"},
							   {"M","V","M","V","M","M"},
							   {"M","M","V","M","M","M"},
							   {"M","M","V","V","M","V"}};


		String[][] exTaulellNouISolucio4 = {{"M","V","V","M","V","M"},
											{"M","M","M","M","M","V"},
											{"V","V","M","M","M","V"},
											{"V","V","M","V","V","M"},
											{"M","V","M","M","V","M"},
											{"M","M","V","V","M","M"}};

		/*Test 5*/
		
		String[][] exAntic5 = {{"M","M","M","M","M"},
							   {"V","M","M","V","M"},
							   {"M","M","M","M","M"},
							   {"V","M","V","V","M"},
							   {"V","M","V","M","M"},
							   {"M","M","M","M","M"},
							   {"M","V","M","M","M"}};

		String[][] exTaulellNouISolucio5 = {{"M","M","M","M","M"},
											{"M","M","M","M","M"},
											{"M","V","V","V","M"},
											{"M","M","V","V","M"},
											{"M","M","V","V","M"},
											{"M","V","M","M","M"},
											{"M","M","M","M","M"}};
		
		/*Test 6*/
		
		String[][] exAntic6 = {{"V","M","V","M","V","M"},
							   {"V","V","V","V","M","M"},
							   {"V","V","V","V","M","M"},
							   {"M","M","M","M","M","V"},
							   {"V","M","M","V","M","V"},
							   {"M","M","M","V","M","M"},
							   {"V","M","V","V","V","M"},
							   {"V","V","M","M","M","V"}};

		String[][] exTaulellNouISolucio6 = {{"V","M","V","M","M","M"},
											{"M","M","M","M","V","M"},
											{"V","M","M","V","V","M"},
											{"V","M","M","V","M","M"},
											{"M","M","M","M","M","M"},
											{"M","V","M","M","M","M"},
											{"V","M","V","V","V","M"},
											{"V","V","V","V","V","M"}};
		
		/*Test 7*/
		
		String[][] exAntic7 = {{"M","V","M","V","M","V","M","M"},
							   {"M","V","V","M","M","M","M","M"},
							   {"M","V","M","M","M","M","V","M"},
							   {"M","M","V","M","M","V","M","V"},
							   {"M","M","M","M","M","M","V","M"},
							   {"M","M","V","M","M","M","V","M"}};

		String[][] exTaulellNouISolucio7 = {{"M","V","M","M","M","M","M","M"},
											{"V","V","M","M","M","M","M","M"},
											{"M","V","M","M","M","M","V","M"},
											{"M","M","M","M","M","V","M","V"},
											{"M","M","M","M","M","V","V","V"},
											{"M","M","M","M","M","M","M","M"}};
		
		/*Test 8*/
		
		String[][] exAntic8 = {{"V","M","M","V","V","V","V"},
							   {"M","V","M","M","M","M","M"},
							   {"V","V","V","V","M","V","M"},
							   {"V","V","V","M","V","M","M"},
							   {"M","M","V","M","V","M","M"},
							   {"M","M","V","M","V","V","M"},
							   {"M","V","V","M","V","M","M"}};
				
		String[][] exTaulellNouISolucio8 = {{"M","M","M","M","V","V","M"},
											{"M","M","M","M","M","M","V"},
											{"M","M","M","V","V","M","M"},
											{"V","M","M","M","V","V","M"},
											{"M","M","V","M","V","M","M"},
											{"M","M","V","M","V","V","M"},
											{"M","V","V","M","V","V","M"}};
		
		/*Test 9*/

		String[][] exAntic9 = {{"M","V","V","M","V","V","M","V","V"},
							   {"M","V","V","M","V","M","M","M","V"},
							   {"M","V","M","M","M","M","M","V","M"},
							   {"M","V","M","V","M","M","V","V","V"},
							   {"V","M","V","M","M","M","V","M","V"},
							   {"V","V","M","M","V","M","M","M","V"}};
			
		String[][] exTaulellNouISolucio9 = 	{{"M","V","V","M","V","V","M","V","V"},
											 {"V","M","M","M","V","V","V","M","V"},
											 {"V","V","M","V","M","M","V","M","M"},
											 {"V","V","M","M","M","M","V","M","V"},
											 {"V","M","V","V","M","V","V","M","V"},
											 {"V","V","M","M","M","M","M","V","M"}};

		/*Test 10*/

		String[][] exAntic10 = {{"M","M","M","V","M"},
								{"M","M","M","V","M"},
								{"M","M","M","M","V"},
								{"M","V","M","M","M"},
								{"M","V","M","M","M"},
								{"V","V","V","V","V"},
								{"M","M","V","V","M"},
								{"M","M","M","M","M"},
								{"M","V","V","M","M"}};

		String[][] exTaulellNouISolucio10 = {{"M","M","M","M","M"},
											 {"M","M","M","V","V"},
											 {"M","M","M","M","M"},
											 {"M","M","M","M","M"},
											 {"M","M","M","V","M"},
											 {"V","M","M","M","V"},
											 {"M","M","M","M","V"},
											 {"M","V","M","V","M"},
											 {"M","M","M","M","M"}};
				
		return Arrays.asList(new Object[][] {
			{exAntic1, 6, 6, exTaulellNouISolucio1}, 
			{exAntic2, 4, 3, exTaulellNouISolucio2},
			{exAntic3, 6, 7, exTaulellNouISolucio3},
			{exAntic4, 6, 6, exTaulellNouISolucio4},
			{exAntic5, 7, 5, exTaulellNouISolucio5},
			{exAntic6, 8, 6, exTaulellNouISolucio6},
			{exAntic7, 6, 8, exTaulellNouISolucio7},
			{exAntic8, 7, 7, exTaulellNouISolucio8},
			{exAntic9, 6, 9, exTaulellNouISolucio9},
			{exAntic10, 9, 5, exTaulellNouISolucio10}
		});
		
	}
	
	/**
	 * Funci� on es compara la soluci� que aporta el m�tode que estem comprovant, amb la soluci� que li donem nosaltres.
	 */

	@Test
	public void test() {
		String[][] res = Programa.evoluciona(taulellAntic, files, columnes);
		assertArrayEquals(solucio,  res);
	}
	
}











