package elJocDeLaVidaProject;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

/**
 * Projecte del Joc de la Vida amb JavaDoc
 * @author Arnau
 * @version 1.0
 */

public class Programa {
	
	static Scanner reader = new Scanner(System.in);

	/**
	 * Metode distribuidor dels altres metodes. Aqui es criden a la resta de metodes.
	 * @param args parametre intern
	 */
	
	public static void main(String[] args) {
		
		ArrayList valors = null;
		
		boolean check = false;
		
		int opcio;
		
		do {
			opcio = menu();
			
			switch (opcio) {
			
				case 1: valors = inicialitzarTauler();
						check = true;
						break;

				case 2: if (check) mostrarTaulell((String[][])valors.get(0), (int) valors.get(1), (int)valors.get(2));
						else System.out.println("Has d'inicialitzar el taulell a l'opci� 1.\n\n");
						break;
					
				case 3: if (check) {
							int numeroIteracions = demanarNumeroIteracions();
							itarar((String[][])valors.get(0), (int) valors.get(1), (int)valors.get(2), numeroIteracions);
						}
						else System.out.println("Has d'inicialitzar el taulell a l'opci� 1.\n\n");
						break;
				
				case 4: 
						break;
						
				case 0: System.out.println("Moltes grcies per jugar\n\nAdeu!!!");
						break;
						
				default: System.out.println("Error, opcio incorrecta. Torna-ho a provar...");
			}
			
		} while (opcio != 0);
	}

	/**
	 * Es demana una opcio i es mostra el menu del programa
	 * @return retornem l'opcio validada
	 */
	
	public static int menu() {
		// TODO Auto-generated method stub
		int op = 0;
		boolean valid = false;
		
		System.out.println("1.- Incialitzar taulell");
		System.out.println("2.- Visualitzar Taulell");
		System.out.println("3.- Iterar");
		System.out.println("0.- Sortir");
		System.out.print("\nEscull una opcio: ");
		
		do {
			try {
				op = reader.nextInt();
				valid = true;
			}
			catch (Exception e) {
				System.out.println("Error, has de posar un numero enter");
				reader.nextLine();
			}
		} while(!valid);
		
		System.out.println("\n");
		
		return op;
	}
	
	/**
	 * Metode on es demana un numero d'iteracions que despres ser� validad per cumplir uns limits 
	 * @return retorna un numero d'iteracions validad
	 */
	
	public static int demanarNumeroIteracions() {
		
		boolean valid = false;
		
		int num = 0;
		
		do {
			try {
				System.out.print("Numero d'iteracions entre 0 i 100: ");
				num = reader.nextInt();
				valid = true;
			}
			catch (Exception e) {
				System.out.println("Error, has de posar un numero enter.");
				reader.nextLine();
			}
		} while(!valid || (num<0 || num>100));

		System.out.println();	

		return num;

	}

	/**
	 * Metode distribuidor d'altres metodes encarregats d'evolucinar la partida
	 * @param taulellAntic taulell el qual iterarem i modificarem
	 * @param files numero de files
	 * @param columnes numero de columnes
	 * @param numeroIteracions numero de vegades que realitzarem l'iteracio
	 * @return retorna el taulell actualitzat amb l'ultima iteracio
	 */
	
	public static String[][] itarar(String[][] taulellAntic, int files, int columnes, int numeroIteracions) {
		//junit
		mostrarTaulell(taulellAntic, files, columnes);
		
		String[][] taulellNou = new String[files][columnes];
		
		for (int i = 0; i < numeroIteracions; i++) {
			
			taulellNou = evoluciona(taulellAntic, files, columnes); //Evolocuionar cel�lules
			
			taulellAntic = actualitzar(taulellNou, taulellAntic, files, columnes); //Copiar taulell nou a taulell antic
			
			mostrarTaulell(taulellNou, files, columnes);
			
		}
	
		return taulellNou;
	}
	
	/**
	 * Fem l'evolucio del taullell segons les regles del joc de la vida
	 * @param taulellAntic taulell antic (el de mostra per comprar.lo)
	 * @param files tamany files
	 * @param columnes tamany columnes
	 * @return retornem el taulell nou actualitzat
	 */
	
	public static String[][] evoluciona(String[][] taulellAntic, int files, int columnes) {
		//juinit
		String [][] taulellNou = new String[files][columnes];
		
		for (int i = 0; i < files; i++) {
			
			for (int j = 0; j < columnes; j++) {
				
				int cont = 0;				

				if (i-1 >=0 && j-1 >=0 && taulellAntic [i-1][j-1].equals("V")) cont++; //System.out.println("[i-1][j-1]");
				if (i-1 >=0 && taulellAntic [i-1][j].equals("V")) cont++;				//System.out.println("[i-1][j]");
				if (i-1 >=0 && j+1 < columnes && taulellAntic [i-1][j+1].equals("V")) cont++; //System.out.println("[i-1][j+1]");
				if (j-1 >=0 && taulellAntic [i][j-1].equals("V")) cont++;					//System.out.println("[i][j-1]");
				if (j+1 < columnes && taulellAntic [i][j+1].equals("V")) cont++;			//System.out.println("[i][j+1]");
				if (i+1 < files && j-1 >=0 && taulellAntic [i+1][j-1].equals("V")) cont++;		//System.out.println("[i+1][j-1]");
				if (i+1 < files && taulellAntic [i+1][j].equals("V")) cont++;					//System.out.println("[i+1][j]");
				if (i+1 < files && j+1 < columnes && taulellAntic [i+1][j+1].equals("V")) cont++;	//System.out.println("[i+1][j+1]");
				
				if (taulellAntic[i][j].equals("M")) {
					if (cont==3) taulellNou[i][j] = "V";
					else taulellNou[i][j] = "M"; 
				}
				
				else {
					if (cont==2 || cont==3) taulellNou[i][j] = "V";
					else taulellNou[i][j] = "M"; 
				}
				
			}
			
		}
		return taulellNou;  
	}

	/**
	 * Metode per actualitzar el taulell antic per el 
	 * @param taulellNou taulell actualitzat
	 * @param taulellAntic taulell no actualitzat
	 * @param files tamany files
	 * @param columnes tamany columnes
	 * @return retorna el taulell antic amb el taulell 
	 */
	
	public static String[][] actualitzar(String[][] taulellNou, String[][] taulellAntic, int files, int columnes) {
		//Junit
		for (int i = 0; i < files; i++) {
			for (int j = 0; j < columnes; j++) {
				taulellAntic[i][j] = taulellNou[i][j];
			}
		}
		
		return taulellAntic;
	}
	
	/**
	 * Depenent del parametre (i) es demanaran les dimensions de les files o les dimensions de les columnes
	 * @param i si es un 1, et demanar� les files i si poses un altre numero et demanara columnes
	 * @return retorna la dimensio de fila o columna validada
	 */
	
	public static int demanarDimensioFilaColumna(int i) {
		
		boolean valid = false;
		
		int num = 0;
		
		do {
			
			try {
				
				if (i==1) System.out.print("Numero entre el 4 i el 10 per definir la fila: ");
				else System.out.print("Numero entre el 4 i el 10 per definir la columna: ");
				num = reader.nextInt();
				valid = true;
			}
			catch (Exception e) {
				System.out.println("Error, has de posar un numero enter.");
				reader.nextLine();
			}
			
		} while(!valid || (num<4 || num>10));
		
		System.out.println();
		
		return num;
	}
	
	/**
	 * Creacio del taulall amb totes les caselles mortes
	 * @param files tamany de les files
	 * @param columnes tamany de les columnes
	 * @return retorna el taulell inicialitzat amb caselles mortes
	 */
	
	public static String[][] CrearTaulell(int files, int columnes) {
		
		String[][] taulell = new String[files][columnes];
		
		for (int i = 0; i < files; i++) {
			for (int j = 0; j < columnes; j++) {
				taulell[i][j] = "M";
			}
		}

		//Activar per posar les cel�lules mortes
		taulell = posarCelulesVives(taulell, files, columnes);
		
		return taulell;
	}
	
	/**
	 * Metode on es col�loquen cel�lules aleatoriament al taulell
	 * @param taulell taullell anteriorment inicialitzat
	 * @param files tamany de les files
	 * @param columnes tamany de les columnes
	 * @return retorna el taulell amb les cel�lules col�locades.
	 * */
	
	public static String[][] posarCelulesVives(String[][] taulell, int files, int columnes) {
		
		Random r = new Random();

		int numeroDeCelulesVives = r.nextInt((files*columnes)/2 - (files*columnes)/4) + (files*columnes)/4; //Funcio aleatoria entre el 1/4 i 1/2 
		
		while (numeroDeCelulesVives>0) {
			
			int posicioFila = (int) (Math.random()*files);
			
			int posicioCol = (int) (Math.random()*columnes);
			
			if (taulell[posicioFila][posicioCol].equals("M")) {
				taulell[posicioFila][posicioCol] = "V";
				numeroDeCelulesVives--;
			}

		}
		
		return taulell;
	}
	
	/**
	 * Metode per inicialitzar el taulell, si "descomentem" el metode crearTaulellAmbFiguraFeta, es modificara el tauell inicialitzat
	 * @return retorna una arrayList amb les files, columnes i el taulell (modificat o no).
	 * */
	
	public static ArrayList inicialitzarTauler() {
		
		ArrayList valorsCrearTaulell = new ArrayList();
		
		int files = demanarDimensioFilaColumna(1);
		
		int columnes = demanarDimensioFilaColumna(2);
		
		String[][] taulell = CrearTaulell(files, columnes); //Retorna taulell amb totes mortes
		
		//Activar per escriure una figura d'exemple.
		//taulell = CrearTaulellAmbFiguraFeta(taulell, files, columnes);
			
		valorsCrearTaulell.add(taulell);
		valorsCrearTaulell.add(files);
		valorsCrearTaulell.add(columnes);
		
		return valorsCrearTaulell;
		
	}

	/**
	 * Metode encarregat de crear un taulell amb la figura d'exemple.
	 * @param taulell taulell anteiorment inicialitzat
	 * @param files tamany files
	 * @param columnes tamany columnes
	 * @return retorna el taulell amb una forma per defecte.
	 */
	
	public static String[][] CrearTaulellAmbFiguraFeta(String[][] taulell, int files, int columnes) {
		
		//String[][] taulell = new String[files][columnes];
		
		taulell[1][3] = "V";
		taulell[1][4] = "V";
		taulell[1][5] = "V";
		taulell[3][7] = "V";
		taulell[4][7] = "V";
		taulell[5][7] = "V";
		taulell[7][3] = "V";
		taulell[7][4] = "V";
		taulell[7][5] = "V";
		taulell[3][1] = "V";
		taulell[4][1] = "V";
		taulell[5][1] = "V";
		
		return taulell;
	}
	
	/**
	 * Imprimeix el taulell per pantalla
	 * @param taulell taulell a mostrar
	 * @param files numero de files
	 * @param columnes numero de columnes
	 */
	
	public static void mostrarTaulell(String[][] taulell, int files, int columnes) {

		System.out.print("   ");
		
		for (int i = 0; i < columnes; i++) System.out.print(" "+i+" ");
		
		System.out.println();
		
		for (int i = 0; i < files; i++) {
			
			System.out.print(" "+i+" ");
			
			for (int j = 0; j < columnes; j++) {
				switch(taulell[i][j]) {
					case "M":	System.out.print(" M ");
								break;
					case "V":	System.out.print(" V ");
								break;	
				}
			}
			
			System.out.println();
			
		}
		
		System.out.println();
	}
	
}
