package elJocDeLaVidaProject;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * Suite que agrupa diferents test.
 * @author arnau
 * @version 1.0
 */

@RunWith(Suite.class)
@SuiteClasses({ProgramaTestActualitzarTaulell.class, ProgramaTestIterar.class, ProgramaTestEvoluciona.class})
public class AllTests {

}
