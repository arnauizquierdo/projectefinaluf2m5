<h2>1.- Objectius</h2>

Els objectius d’aquesta pràctica són:

<ul>
<li>Conèixer i practicar les eines que permeten fer proves unitàries d’un programa o mètode (funció), així com entendre la seva importància.</li>

<li>Conèixer i practicar les eines que permeten fer la documentació d’un programa.</li>

<li>Conèixer i practicar les eines que permeten gestionar versions dels nostres programes, així com entendre la seva importància.</li>
    
<li>Continuar millorant com a programadors, desenvolupant un programa que servirà per poder aplicar tots els punts anteriors.</li>

</ul>

<h2>2.- Enunciat del problema</h2>

Aplicant els conceptes de la programació descendent, fes un programa que doni sol.lució al següent enunciat.

https://docs.google.com/document/d/17l4HahmZcHDOlXqoWB6R4yIXIr5oI1763LG6mEwgzFo/edit

Els mètodes que desenvolupis, sempre que sigui posible, hauran de retornar un valor, matriu, string, etc. Intenteu no fer servir mètodes void.

<h2>3.-	Control de Versions amb Git</h2>

Farem el control de versions amb Git. Les últimes versions d’Eclipse ja porten el Git incorporat i no cal fer res adicional.

Heu de mantenir el vostre projecte amb git, controlant le seves versions. Ho hauràs de fer amb les comandes necessàries.

Llegeix-te aquest manual d’integració d’Eclipse amb GitHub / GitLab.

https://www.redeszone.net/2013/04/19/como-utilizar-github-para-sincronizar-procesos-de-eclipse-con-otros-usuarios%EF%BB%BF%EF%BB%BF/

Crea un nou projecte amb Gitlab, que es sincronitza amb el teu git local i cada cop que facis modificacions al teu codi, puja-les al teu gitlab.

Quan finalitzis tot el projecte, puja com a part de la  sol.lució el link al teu projecte en Gitlab

<h2>4.-	Documentació amb Javadoc</h2>

Javadoc és una utilitat d’Oracle per a la generació de documentació d’APIs en format HTML a partir de codi Java. Javadoc es l’estàndard de la indústria per documentar classes de Java.

Es poden trobar bons exemples de javadoc en la Wikipedia: https://en.wikipedia.org/wiki/Javadoc

Feu el Javadoc per al vostre projecte. Feu tant el principal com per a tots els mètodes. Genereu l’ HTML i adjunteu-lo.

<h2>5.-	Proves de Caixa Negra.  JUnit</h2>

JUnit és una de les llibreries externes de Java més utilitzades, i serveix per generar i gestionar casos de prova dels nostres programes i funcions.

JUnit és un entorn de treball que permet executar les classes Java de manera controlada, de forma que es pot avaluar si el funcionament de cadascun dels mètodes de la classe es comporta com s'espera. La seva funció és, donat un valor d'entrada, analitzar si el valor de retorn és el que s'espera, si la classe compleix amb l'especificació, aleshores JUnit indicarà que el mètode de la classe ha passat la prova; en cas que el valor obtingut sigui diferent a l’esperat, JUnit indicarà una fallada en el mètode corresponent.

Hauríeu de tenir JUnit integrat a Eclipse. En cas contrari o nova instal.lació, seguiu les instruccions següents:

https://www.tutorialspoint.com/junit/junit_plug_with_eclipse.htm

<ul>
<li>Crea una classe Test que faci suficients jocs de prova per assegurar-te que la teva solució funciona perfectament.</li>

<li>Fes servir una Suite de proves que llanci automàticament tots els test de tots els mètodes.</li>

<li>Documenta en cada classe test el que s’està provant.</li>

<li>Teniu un tutorial complet sobre JUnit aquí:  https://www.tutorialspoint.com/junit/index.htm</li>

<li>Teniu una activitat específica de jUnit.</li>
</ul>
